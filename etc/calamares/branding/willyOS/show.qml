/* === This file is part of Calamares - <https://calamares.io> ===
 *
 *   SPDX-FileCopyrightText: 2015 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2018 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   Calamares is Free Software: see the License-Identifier above.
 *
 */

import QtQuick 2.0;
import calamares.slideshow 1.0;

Presentation
{
    id: presentation

    function nextSlide() {
        console.log("QML Component (default slideshow) Next slide");
        presentation.goToNextSlide();
    }

    Timer {
        id: advanceTimer
        interval: 10000
        running: presentation.activatedInCalamares
        repeat: true
        onTriggered: nextSlide()
    }

    Slide {

        Image {
            id: background
            source: "slide.png"
            width: 852; height: 480
            /*fillMode: Image.PreserveAspectFit*/
            fillMode: Image.Strech
            anchors.centerIn: parent
        }
       // Text {
       //     anchors.horizontalCenter: background.horizontalCenter
       //     anchors.top: background.bottom
       //     text: "This is a customizable QML slideshow.<br/>"+
       //           "Distributions should provide their own slideshow and list it in <br/>"+
       //           "their custom branding.desc file.<br/>"+
       //           "To create a Calamares presentation in QML, import calamares.slideshow,<br/>"+
       //           "define a Presentation element with as many Slide elements as needed."
       //     wrapMode: Text.WordWrap
       //    width: presentation.width
       //     horizontalAlignment: Text.Center
       // }
    }

    // When this slideshow is loaded as a V1 slideshow, only
    // activatedInCalamares is set, which starts the timer (see above).
    //
    // In V2, also the onActivate() and onLeave() methods are called.
    // These example functions log a message (and re-start the slides
    // from the first).
    function onActivate() {
        console.log("QML Component (default slideshow) activated");
        presentation.currentSlide = 0;
    }

    function onLeave() {
        console.log("QML Component (default slideshow) deactivated");
    }

}
